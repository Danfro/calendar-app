# Chinese (Simplified) translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 09:07+0000\n"
"PO-Revision-Date: 2021-06-13 20:40+0000\n"
"Last-Translator: Ito Rimai <ItoRimai@foxmail.com>\n"
"Language-Team: Chinese (Simplified) <https://translate.ubports.com/projects/"
"ubports/calendar-app/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: lomiri-calendar-app.desktop.in:7
msgid "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"
msgstr ""

#: lomiri-calendar-app.desktop.in:8 qml/EventDetails.qml:348
#: qml/NewEvent.qml:677
msgid "Calendar"
msgstr "日历"

#: lomiri-calendar-app.desktop.in:9
#, fuzzy
#| msgid "A calendar for Ubuntu which syncs with online accounts."
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr "与在线账户同步的 Ubuntu 日历软件。"

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr ""
"calendar;event;day;week;year;appointment;meeting;日历;会;天;月;年;星期;周;"

#: qml/AgendaView.qml:51 qml/calendar.qml:373 qml/calendar.qml:554
msgid "Agenda"
msgstr "待办事项"

#: qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "您没有启用的日历"

#: qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "没有即将到来的事件"

#: qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "启用日历"

#: qml/AgendaView.qml:206
msgid "no event name set"
msgstr "未设事件名称"

#: qml/AgendaView.qml:208
msgid "no location"
msgstr "未设地点"

#: qml/AllDayEventComponent.qml:89 qml/TimeLineBase.qml:50
msgid "New event"
msgstr "新建事件"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 个事件"

#. TRANSLATORS: the argument refers to the number of all day events
#: qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 全天活动"

#: qml/CalendarChoicePopup.qml:45 qml/EventActions.qml:55
msgid "Calendars"
msgstr "日历"

#: qml/CalendarChoicePopup.qml:47 qml/SettingsPage.qml:51
msgid "Back"
msgstr "返回"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Sync"
msgstr "同步"

#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Syncing"
msgstr "正在同步"

#: qml/CalendarChoicePopup.qml:84
msgid "Add online Calendar"
msgstr "添加在线日历表"

#: qml/CalendarChoicePopup.qml:189
msgid "Unable to deselect"
msgstr "无法取消选择"

#: qml/CalendarChoicePopup.qml:190
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr "要创创建新事件，您必须选中至少一个可写的日历"

#: qml/CalendarChoicePopup.qml:192 qml/CalendarChoicePopup.qml:205
msgid "Ok"
msgstr "确定"

#: qml/CalendarChoicePopup.qml:202
msgid "Network required"
msgstr ""

#: qml/CalendarChoicePopup.qml:203
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""

#: qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "选择颜色"

#: qml/ColorPickerDialog.qml:55 qml/DeleteConfirmationDialog.qml:63
#: qml/EditEventConfirmationDialog.qml:52 qml/NewEvent.qml:394
#: qml/OnlineAccountsHelper.qml:73 qml/RemindersPage.qml:88
msgid "Cancel"
msgstr "取消"

#: qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "无联系人"

#: qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "搜索联系人"

#: qml/DayView.qml:76 qml/MonthView.qml:51 qml/WeekView.qml:60
#: qml/YearView.qml:57
msgid "Today"
msgstr "今天"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: qml/DayView.qml:129 qml/MonthView.qml:84 qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr "月/年"

#: qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "删除循环事件"

#: qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "删除事件"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "仅删除此 \"%1\"活动，还是系列中所有活动？"

#: qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "您确定想要删除 \"%1\" 活动？"

#: qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "删除系列"

#: qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "删除这个"

#: qml/DeleteConfirmationDialog.qml:51 qml/NewEvent.qml:401
msgid "Delete"
msgstr "删除"

#: qml/EditEventConfirmationDialog.qml:29 qml/NewEvent.qml:389
msgid "Edit Event"
msgstr "编辑事件"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "仅编辑此 \"%1\"活动，还是系列中所有活动？"

#: qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "编辑系列"

#: qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "编辑这个"

#: qml/EventActions.qml:67 qml/SettingsPage.qml:49
msgid "Settings"
msgstr "设置"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/EventDetails.qml:37 qml/NewEvent.qml:596
msgid "Event Details"
msgstr "事件详情"

#: qml/EventDetails.qml:40
msgid "Edit"
msgstr "编辑"

#: qml/EventDetails.qml:173 qml/TimeLineHeader.qml:66 qml/calendar.qml:750
msgid "All Day"
msgstr "全天"

#: qml/EventDetails.qml:392
msgid "Attending"
msgstr "参加"

#: qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "不参加"

#: qml/EventDetails.qml:396
msgid "Maybe"
msgstr "可能"

#: qml/EventDetails.qml:398
msgid "No Reply"
msgstr "未回复"

#: qml/EventDetails.qml:437 qml/NewEvent.qml:632
msgid "Description"
msgstr "描述"

#: qml/EventDetails.qml:464 qml/NewEvent.qml:867 qml/NewEvent.qml:884
msgid "Reminder"
msgstr "提醒"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: qml/EventRepetition.qml:40 qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "重复"

#: qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "重复于："

#: qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr "重复间隔"

#: qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "重复事件结束"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: qml/EventRepetition.qml:294 qml/NewEvent.qml:840
msgid "Repeats"
msgstr "重复提醒"

#: qml/EventRepetition.qml:320 qml/NewEvent.qml:494
msgid "Date"
msgstr "日期"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 次"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; 至 %2"

#: qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "；每 %1 天"

#: qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "；每 %1 周"

#: qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "；每 %1 月"

#: qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "；每 %1 年"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "每周 %1"

#: qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "永不"

#: qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "在X次后"

#: qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "起始日期"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: qml/MonthComponent.qml:294
msgid "Wk"
msgstr "周"

#: qml/MonthView.qml:79 qml/WeekView.qml:140
msgid "%1 %2"
msgstr "%1 %2"

#: qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "结束时间不能早于开始时间"

#: qml/NewEvent.qml:389 qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "新建事件"

#: qml/NewEvent.qml:419
msgid "Save"
msgstr "保存"

#: qml/NewEvent.qml:430
msgid "Error"
msgstr "错误"

#: qml/NewEvent.qml:432
msgid "OK"
msgstr "确定"

#: qml/NewEvent.qml:494
msgid "From"
msgstr "从"

#: qml/NewEvent.qml:519
msgid "To"
msgstr "至"

#: qml/NewEvent.qml:548
msgid "All day event"
msgstr "全天事件"

#: qml/NewEvent.qml:585
msgid "Event Name"
msgstr "事件名称"

#: qml/NewEvent.qml:605
msgid "More details"
msgstr "更多细节"

#: qml/NewEvent.qml:656
msgid "Location"
msgstr "位置"

#: qml/NewEvent.qml:744
msgid "Guests"
msgstr "客人"

#: qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "添加被邀请人"

#: qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "选择创建账户"

#: qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "一次"

#: qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "每天"

#: qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "在工作日"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "在%1,%2,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "在%1和%2"

#: qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "每周"

#: qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "每月"

#: qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "每年"

#: qml/RemindersModel.qml:31 qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "暂无提醒"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: qml/RemindersModel.qml:34 qml/RemindersModel.qml:103
msgid "On Event"
msgstr "进行中"

#: qml/RemindersModel.qml:43 qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 周"

#: qml/RemindersModel.qml:54 qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 天"

#: qml/RemindersModel.qml:65 qml/RemindersModel.qml:108
#: qml/SettingsPage.qml:274 qml/SettingsPage.qml:275 qml/SettingsPage.qml:276
#: qml/SettingsPage.qml:277
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1小时"

#: qml/RemindersModel.qml:74 qml/SettingsPage.qml:270 qml/SettingsPage.qml:271
#: qml/SettingsPage.qml:272 qml/SettingsPage.qml:273
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 分钟"

#: qml/RemindersModel.qml:104 qml/RemindersModel.qml:105
#: qml/RemindersModel.qml:106 qml/RemindersModel.qml:107
#: qml/SettingsPage.qml:385 qml/SettingsPage.qml:386 qml/SettingsPage.qml:387
#: qml/SettingsPage.qml:388 qml/SettingsPage.qml:389 qml/SettingsPage.qml:390
#: qml/SettingsPage.qml:391 qml/SettingsPage.qml:392
msgid "%1 minutes"
msgstr "%1分钟"

#: qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr "%1小时"

#: qml/RemindersModel.qml:111
msgid "%1 days"
msgstr "%1 天"

#: qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr "%1周"

#: qml/RemindersModel.qml:114
msgid "Custom"
msgstr "自定义"

#: qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "自定义提醒"

#: qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr "设置提醒"

#: qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "显示周数"

#: qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr "显示农历"

#: qml/SettingsPage.qml:125
msgid "Business hours"
msgstr "工作时间"

#: qml/SettingsPage.qml:232
msgid "Data refresh interval"
msgstr ""

#: qml/SettingsPage.qml:262
msgid ""
"Note: Automatic syncronisation currently only works while the app is open in "
"the foreground. Alternatively set background suspension to off. Then sync "
"will work while the app is open in the background."
msgstr ""

#: qml/SettingsPage.qml:316
msgid "Default reminder"
msgstr "默认提示"

#: qml/SettingsPage.qml:363
msgid "Default length of new event"
msgstr "新事件的默认时长"

#: qml/SettingsPage.qml:426
msgid "Default calendar"
msgstr "默认日历"

#: qml/SettingsPage.qml:495
msgid "Theme"
msgstr "主题"

#: qml/SettingsPage.qml:518
msgid "System theme"
msgstr "系统主题"

#: qml/SettingsPage.qml:519
msgid "SuruDark theme"
msgstr "Suru暗色主题"

#: qml/SettingsPage.qml:520
msgid "Ambiance theme"
msgstr "Ambiance主题"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "W%1"

#: qml/WeekView.qml:147 qml/WeekView.qml:148
msgid "MMM"
msgstr "月"

#: qml/YearView.qml:83
msgid "Year %1"
msgstr "%1 年"

#: qml/calendar.qml:99
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"日历软件有四个参数：--starttime, --endtime, --newevent 和 --eventid，这些参数"
"由系统管理，请查阅源代码了解更多信息"

#: qml/calendar.qml:381 qml/calendar.qml:575
msgid "Day"
msgstr "日"

#: qml/calendar.qml:389 qml/calendar.qml:596
msgid "Week"
msgstr "周"

#: qml/calendar.qml:397 qml/calendar.qml:617
msgid "Month"
msgstr "月"

#: qml/calendar.qml:405 qml/calendar.qml:638
msgid "Year"
msgstr "年"

#~ msgid "5 minutes"
#~ msgstr "5 分钟"

#~ msgid "10 minutes"
#~ msgstr "10 分钟"

#~ msgid "15 minutes"
#~ msgstr "15 分钟"

#~ msgid "30 minutes"
#~ msgstr "30 分钟"

#~ msgid "1 hour"
#~ msgstr "1 小时"

#~ msgid "2 hours"
#~ msgstr "2 小时"

#~ msgid "1 day"
#~ msgstr "1 天"

#~ msgid "2 days"
#~ msgstr "两天"

#~ msgid "1 week"
#~ msgstr "1 周"

#~ msgid "2 weeks"
#~ msgstr "两周"

#~ msgid "Show lunar calendar"
#~ msgstr "农历显示"
