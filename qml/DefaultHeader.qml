/*
* Copyright (C) 2013-2014 Canonical Ltd
*
* This file is part of Lomiri Calendar App
*
* Lomiri Calendar App is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Lomiri Calendar App is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import Lomiri.Components 1.3

PageHeader {
    id: pageHeader

    HeaderSections {
        id: headerSections
        visible: !titleHeaderSections.enoughWidth
    }

    contents: TitleHeaderSections {
        id: titleHeaderSections
    }

    states: [
        State {
            name: "MOBILE"
            when: !titleHeaderSections.enoughWidth
            PropertyChanges {
                target: pageHeader
                extension: headerSections
            }
        }
    ]
}
